/*CaltulatorArg
 * Provides basic calculator functions
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 9.3.2021
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CalculatorSys {
	
	/*
	 * promt takes format-input as String.
	 * reads double form Console. loops until a positive, valid double is entered.
	 */
	public static double readDouble(Scanner scanner, String promt) {
		boolean isDouble = false;
		double input = 0.00;
		do {
			System.out.print(promt);
			
			if(scanner.hasNextFloat()) {
				input = scanner.nextDouble();
				isDouble = true;
				if(!scanner.nextLine().equals("")) {
					isDouble = false;
				}
			}else {
				scanner.nextLine();
			}
			
			if(input < 0) {
				isDouble = false;
			}
			
			if(!isDouble) {
				System.out.println("Please enter a non-negative valid double!");
			}
			
		}while(!isDouble);
			
		return input;
	}
	
	/*
	 * reads operator from Console. loops until a valid operator is entered.
	 * valid operators can be modified inside the method
	 */
	public static String readOperator(Scanner scanner) {
		boolean isOK = false;
		String operator = "";
		
		List<String> validOperators = new ArrayList<String>(List.of("+", "-", "*", "/"));
		
		do {
			System.out.print("Operator: ");
			
			operator = scanner.nextLine().trim();
			
			if(validOperators.contains(operator)) {
				isOK = true;
			}
			
			if(!isOK) {
				System.out.print("Please enter a valid operator - allowed operators: ");
				for(int i=0; i<validOperators.size(); i++) {
					if(i != 0) {
						System.out.print(",");
					}
					System.out.print(validOperators.get(i));
				}
				System.out.printf("\n");
				
			}
			
		}while(!isOK);
		
		return operator;
	}
	
	public static void main(String[] args) {
		
		Locale.setDefault(new java.util.Locale("en","US"));
		
		//Variables
		Scanner scanner = new Scanner(System.in);
		double result = 0.0;
		
		//Program
		double num1 = readDouble(scanner, "Value 1: ");
		String operator = readOperator(scanner);
		double num2 = readDouble(scanner, "Value 2: ");
		
		scanner.close();
		
		switch (operator) {
		case "+":
			result = num1 + num2;
			break;
		case "-":
			result = num1 - num2;
			break;
		case "*":
			result = num1 * num2;
			break;
		case "/":
			result = num1 / num2;
			break;
		}
		
		System.out.printf("%.2f %s %.2f = %.2f", num1, operator, num2, result);
		
	}

}
